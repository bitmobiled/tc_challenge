import 'package:flutter/material.dart';

class AppColors {
  static const Color primary = contentColorCyan;
  static const Color pageBackground = Color(0xFF282E45);

  static const Color contentColorBlue = Color(0xFF2196F3);
  static const Color contentColorOrange = Color(0xFFFF683B);
  static const Color contentColorPink = Color(0xFFFF3AF2);
  static const Color contentColorCyan = Color(0xFF50E4FF);
  static const Color contentColorCyanDark = Color(0xFF249cb3);
}
