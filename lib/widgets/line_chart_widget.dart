import 'package:flutter/material.dart';
import 'package:fl_chart/fl_chart.dart';
import '../theme/colors.dart';

class LineChartWidget extends StatelessWidget {
  final List<FlSpot> debitSpots;
  final List<FlSpot> creditSpots;

  const LineChartWidget({
    Key? key,
    required this.debitSpots,
    required this.creditSpots,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return LineChart(
      accountChartData,
      duration: const Duration(milliseconds: 250),
    );
  }

  LineChartData get accountChartData => LineChartData(
        lineTouchData: lineTouchData1,
        gridData: gridData,
        titlesData: titlesData1,
        borderData: borderData,
        lineBarsData: lineBarsData1,
        minX: 6,
        maxX: 10,
        maxY: 4,
        minY: 0,
      );

  // Configure touch behavior of the chart
  LineTouchData get lineTouchData1 => LineTouchData(
        handleBuiltInTouches: true,
        touchTooltipData: LineTouchTooltipData(
          tooltipBgColor: Colors.blueGrey.withOpacity(0.8),
        ),
      );

  // Configure titles for the axes of the chart
  FlTitlesData get titlesData1 => FlTitlesData(
        bottomTitles: AxisTitles(
          sideTitles: bottomTitles,
        ),
        rightTitles: const AxisTitles(
          sideTitles: SideTitles(showTitles: false),
        ),
        topTitles: const AxisTitles(
          sideTitles: SideTitles(showTitles: false),
        ),
        leftTitles: AxisTitles(
          sideTitles: leftTitles(),
        ),
      );

  // Configure line data for the chart
  List<LineChartBarData> get lineBarsData1 => [
        lineChartBarDataCredit,
        lineChartBarDataDebit,
      ];

  // Custom function to display left axis titles
  Widget leftTitleWidgets(double value, TitleMeta meta) {
    const style = TextStyle(
      fontWeight: FontWeight.normal,
      fontSize: 14,
    );
    String text;
    switch (value.toInt()) {
      case 1:
        text = '1k';
        break;
      case 2:
        text = '2k';
        break;
      case 3:
        text = '3k';
        break;
      case 4:
        text = '4k';
        break;
      default:
        return Container();
    }

    return Text(text, style: style, textAlign: TextAlign.center);
  }

  // Configure left titles for the axis
  SideTitles leftTitles() => SideTitles(
        getTitlesWidget: leftTitleWidgets,
        showTitles: true,
        interval: 1,
        reservedSize: 40,
      );

  // Configure bottom titles for the axis
  SideTitles get bottomTitles => SideTitles(
        showTitles: true,
        reservedSize: 32,
        interval: 1,
        getTitlesWidget: bottomTitleWidgets,
      );

  // Custom function to display bottom axis titles
  Widget bottomTitleWidgets(double value, TitleMeta meta) {
    const style = TextStyle(
      fontWeight: FontWeight.bold,
      fontSize: 12,
      color: Colors.amber,
    );
    Widget text;
    switch (value.toInt()) {
      case 6:
        text = const Text('JUN', style: style);
        break;
      case 7:
        text = const Text('JUL', style: style);
        break;
      case 8:
        text = const Text('AUG', style: style);
        break;
      case 9:
        text = const Text('SEPT', style: style);
        break;
      case 10:
        text = const Text('OCT', style: style);
        break;
      default:
        text = const Text('');
        break;
    }

    return SideTitleWidget(
      axisSide: meta.axisSide,
      space: 10,
      child: text,
    );
  }

  // Configure grid data for the chart
  FlGridData get gridData => const FlGridData(show: false);

  // Configure border data for the chart
  FlBorderData get borderData => FlBorderData(
        show: true,
        border: Border(
          bottom:
              BorderSide(color: AppColors.primary.withOpacity(0.2), width: 2),
          left: BorderSide(color: AppColors.primary.withOpacity(0.2), width: 2),
          right: const BorderSide(color: Colors.transparent),
          top: const BorderSide(color: Colors.transparent),
        ),
      );

  // Configure line chart data for credits
  LineChartBarData get lineChartBarDataCredit => LineChartBarData(
        isCurved: true,
        color: AppColors.contentColorBlue,
        barWidth: 3,
        isStrokeCapRound: true,
        dotData: const FlDotData(show: false),
        belowBarData: BarAreaData(show: false),
        spots: creditSpots,
      );

  // Configure line chart data for debits
  LineChartBarData get lineChartBarDataDebit => LineChartBarData(
        isCurved: true,
        color: AppColors.contentColorOrange,
        barWidth: 3,
        isStrokeCapRound: true,
        dotData: const FlDotData(show: false),
        belowBarData: BarAreaData(
          show: false,
          color: AppColors.contentColorPink.withOpacity(0),
        ),
        spots: debitSpots,
      );
}
