import 'package:flutter/material.dart';
import 'package:tc_challenge/theme/colors.dart';

class NotificationIcon extends StatelessWidget {
  const NotificationIcon({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 40,
      height: 60,
      child: Stack(
        children: [
          // Notification icon placed at the center of the container
          const Align(
            alignment: Alignment.center,
            child: Icon(
              Icons.notifications,
              size: 30,
              color: Colors.white,
            ),
          ),
          // Positioned widget to place the notification count on the top right
          Positioned(
            top: 8,
            right: 4,
            child: Container(
              padding: const EdgeInsets.all(2),
              decoration: const BoxDecoration(
                shape: BoxShape.circle,
                color: AppColors.contentColorOrange,
              ),
              // Text widget displaying the notification count
              child: const Text(
                '10',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 12,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
