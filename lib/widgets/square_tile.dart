import 'package:flutter/material.dart';

class SquareTile extends StatefulWidget {
  final String imagePath;
  final VoidCallback? onTap;

  const SquareTile({
    Key? key,
    required this.imagePath,
    this.onTap,
  }) : super(key: key);

  @override
  _SquareTileState createState() => _SquareTileState();
}

class _SquareTileState extends State<SquareTile> {
  bool isPressed = false;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTapDown: (_) {
        setState(() {
          isPressed = true;
        });
      },
      onTapUp: (_) {
        setState(() {
          isPressed = false;
        });
        // Call the provided onTap function when the tile is released
        widget.onTap?.call();
      },
      onTapCancel: () {
        setState(() {
          isPressed = false;
        });
      },
      child: Opacity(
        opacity: isPressed ? 0.8 : 1.0,
        child: Container(
          padding: const EdgeInsets.all(18),
          decoration: BoxDecoration(
            border: Border.all(color: Colors.white),
            borderRadius: BorderRadius.circular(16),
            color: Colors.grey[200],
            boxShadow: [
              BoxShadow(
                color: Colors.black.withOpacity(0.1),
                offset: const Offset(0, 2),
                blurRadius: 4,
              ),
            ],
          ),
          child: Image.asset(
            widget.imagePath,
            height: 30,
          ),
        ),
      ),
    );
  }
}
