import 'package:flutter/material.dart';

class CredentialsErrorDialog extends StatelessWidget {
  const CredentialsErrorDialog({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: const Text(
        'Incorrect Credentials',
        style: TextStyle(
          color: Colors.red, // Title text color
        ),
      ),
      content: const Text(
        'Please enter valid username and password.',
        style: TextStyle(
          color: Colors.black, // Content text color
        ),
      ),
      backgroundColor: Colors.white, // Background color
      actions: <Widget>[
        TextButton(
          onPressed: () {
            Navigator.of(context).pop(); // Close the dialog
          },
          child: Container(
            decoration: BoxDecoration(
              color: Colors.grey.withOpacity(0.3), // Button background color
              borderRadius: BorderRadius.circular(8), // Button border radius
            ),
            padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8), // Button padding
            child: const Text(
              'OK',
              style: TextStyle(
                color: Colors.black, // Button text color
              ),
            ),
          ),
        ),
      ],
    );
  }
}
