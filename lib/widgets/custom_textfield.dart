import 'package:flutter/material.dart';

class CustomTextField extends StatefulWidget {
  final TextEditingController controller; // Controller for text editing
  final String hintText; // Placeholder text when the field is empty
  final bool obscureText; // Determines whether the text is obscured (e.g., for passwords)

  const CustomTextField({
    Key? key,
    required this.controller,
    required this.hintText,
    required this.obscureText,
  }) : super(key: key);

  @override
  _CustomTextFieldState createState() => _CustomTextFieldState();
}

class _CustomTextFieldState extends State<CustomTextField> {
  late bool obscureText; // Tracks whether the text is obscured or not

  @override
  void initState() {
    super.initState();
    obscureText = widget.obscureText; // Initialize the value of obscureText
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 25.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            decoration: BoxDecoration(
              border: Border.all(color: Colors.grey.shade400),
              borderRadius: BorderRadius.circular(8),
              color: Colors.white,
            ),
            child: Row(
              children: [
                Padding(
                  padding: const EdgeInsets.fromLTRB(8.0, 8, 0, 8),
                  child: Icon(
                    widget.obscureText ? Icons.lock : Icons.person, // Display icon based on obscureText value
                    color: Colors.grey[500],
                  ),
                ),
                Expanded(
                  child: TextField(
                    controller: widget.controller, // Set the controller for text editing
                    obscureText: obscureText, // Set text visibility based on obscureText value
                    decoration: InputDecoration(
                      contentPadding: const EdgeInsets.symmetric(vertical: 18.0, horizontal: 16),
                      border: InputBorder.none,
                      hintText: widget.hintText, // Display the provided hint text
                      hintStyle: TextStyle(color: Colors.grey[500]),
                    ),
                  ),
                ),
                if (widget.obscureText)
                  GestureDetector(
                    onTap: () {
                      setState(() {
                        obscureText = !obscureText; // Toggle obscureText value on tap
                      });
                    },
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Icon(
                        obscureText ? Icons.visibility_off : Icons.visibility,
                        color: Colors.grey[500],
                      ),
                    ),
                  ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
