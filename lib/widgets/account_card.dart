import 'package:flutter/material.dart';
import '../models/user_account.dart'; // Importing the UserAccount model

class AccountCard extends StatefulWidget {
  final UserAccount userAccount; // UserAccount object to display data
  final void Function(UserAccount) onCardPressed; // Callback function when card is pressed

  const AccountCard({
    Key? key,
    required this.userAccount,
    required this.onCardPressed, // Accept callback function
  }) : super(key: key);

  @override
  _AccountCardState createState() => _AccountCardState();
}

class _AccountCardState extends State<AccountCard>
    with SingleTickerProviderStateMixin {
  bool _isTapped = false; // State variable to track card tap

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        setState(() {
          _isTapped = !_isTapped; // Toggle tapped state
        });
        // When the card is tapped, invoke the callback function
        widget.onCardPressed(widget.userAccount); // Pass the user account data
      },
      onTapCancel: () {
        setState(() {
          _isTapped = false; // Reset tapped state on tap cancel
        });
      },
      onTapDown: (_) {
        setState(() {
          _isTapped = true; // Set tapped state on tap down
        });
      },
      onTapUp: (_) {
        setState(() {
          _isTapped = false; // Reset tapped state on tap up
        });
      },
      child: AnimatedOpacity(
        opacity: _isTapped ? 0.8 : 1.0, // Change the opacity value as needed
        duration: const Duration(milliseconds: 300), // Duration for opacity animation
        child: Card(
          elevation: 3, // Set the card elevation
          margin: const EdgeInsets.all(8), // Set margin for the card
          color: Colors.lightBlue[50], // Set the light blue background color
          child: Padding(
            padding: const EdgeInsets.all(16), // Padding for content inside the card
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'Account Type: ${widget.userAccount.accountType} ',
                  style: const TextStyle(
                    fontSize: 18,
                    // fontWeight: FontWeight.bold,
                  ),
                ),
                const SizedBox(height: 8), // Adding space between elements
                Row(
                  children: [
                    const Text(
                      'Available Balance: ',
                      style: TextStyle(
                        fontSize: 16,
                        color: Colors.black, // Change the color to your desired color
                        // Add more styles as needed
                      ),
                    ),
                    Text(
                      '\$${widget.userAccount.availableBalance.toStringAsFixed(2)}',
                      style: const TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                        color: Colors.blue, // Change the color to your desired color
                        // Add more styles as needed
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
