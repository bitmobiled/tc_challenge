import 'package:flutter/material.dart';
import '../widgets/square_tile.dart';

class SocialSignInButtons extends StatelessWidget {
  const SocialSignInButtons({Key? key});

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        // Google Sign-In button
        SquareTile(
          imagePath: 'lib/assets/images/google.png',
          onTap: () {
            // Handle Google button tap
            print('Google button tapped!');
          },
        ),
        const SizedBox(width: 25),
        // Facebook Sign-In button
        SquareTile(
          imagePath: 'lib/assets/images/facebook.png',
          onTap: () {
            // Handle Facebook button tap
            print('Facebook button tapped!');
          },
        ),
        const SizedBox(width: 25),
        // Apple Sign-In button
        SquareTile(
          imagePath: 'lib/assets/images/apple.png',
          onTap: () {
            // Handle Apple button tap
            print('Apple button tapped!');
          },
        ),
      ],
    );
  }
}
