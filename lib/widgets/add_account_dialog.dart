import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tc_challenge/theme/colors.dart';
import 'package:tc_challenge/utils/random_account.dart';

import '../models/user_account.dart';
import '../providers/user_account_provider.dart';

class AddAccountDialog extends StatefulWidget {
  const AddAccountDialog({Key? key}) : super(key: key);

  @override
  _AddAccountDialogState createState() => _AddAccountDialogState();
}

class _AddAccountDialogState extends State<AddAccountDialog> {
  String selectedAccountType = 'Savings'; // Default account type

  // Function to handle the "Save" button press
  void onSaveButtonPressed() {
    // Generate a random availableBalance between 1000 and 50000
    UserAccount userAccount = generateAccounts(selectedAccountType);

    // Access the UserAccountProvider using Provider
    UserAccountProvider userAccountProvider =
    Provider.of<UserAccountProvider>(context, listen: false);

    // Add the new user account to the provider
    userAccountProvider.addUserAccount(userAccount);

    // Close the dialog
    Navigator.of(context).pop();
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      backgroundColor: Colors.white,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      title: const IntrinsicHeight(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Add Account',
              style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.bold,
                fontSize: 18.0,
              ),
            ),
            Divider(
              color: Colors.grey,
              thickness: 1.0,
            ),
          ],
        ),
      ),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          const Text(
            'Select Account Type:',
            style: TextStyle(color: Colors.black),
          ),
          const SizedBox(height: 10),
          // Dropdown for selecting account type
          SizedBox(
            height: 44.0, // Adjust the height as needed
            child: Container(
              decoration: BoxDecoration(
                border: Border.all(
                  color: Colors.grey.withOpacity(0.6), // Border color
                  width: 1.0, // Border width
                ),
                borderRadius: BorderRadius.circular(8.0), // Border radius
              ),
              padding: const EdgeInsets.symmetric(horizontal: 16.0), // Horizontal padding
              child: DropdownButton<String>(
                value: selectedAccountType,
                onChanged: (String? value) {
                  if (value != null) {
                    setState(() {
                      selectedAccountType = value;
                    });
                  }
                },
                style: const TextStyle(color: Colors.black, fontSize: 16), // Style for the selected value
                underline: Container(
                  height: 0, // Remove the default underline
                ),
                items: <String>[
                  'Savings',
                  'Current',
                  'Fixed Deposit',
                  'Nominated',
                  'Personal'
                ].map<DropdownMenuItem<String>>((String value) {
                  return DropdownMenuItem<String>(
                    value: value,
                    child: Padding(
                      padding: const EdgeInsets.symmetric(vertical: 2.0),
                      child: Text(
                        value,
                        style: const TextStyle(color: Colors.black, fontSize: 16), // Style for the dropdown items
                      ),
                    ),
                  );
                }).toList(),
              ),
            ),
          ),
        ],
      ),
      actions: [
        // Save Button
        Container(
          margin: const EdgeInsets.fromLTRB(0, 0, 8.0, 4),
          decoration: BoxDecoration(
            border: Border.all(color: AppColors.contentColorCyanDark, width: 1.0),
            borderRadius: BorderRadius.circular(5.0),
          ),
          child: SizedBox(
            height: 32.0,
            child: TextButton(
              onPressed: onSaveButtonPressed, // Call the function on button press
              child: const Text('Save', style: TextStyle(color: AppColors.contentColorCyanDark)),
            ),
          ),
        ),
        // Cancel Button
        Container(
          margin: const EdgeInsets.fromLTRB(0, 0, 12, 4),
          decoration: BoxDecoration(
            border: Border.all(color: Colors.black87, width: 1.0),
            borderRadius: BorderRadius.circular(5.0),
          ),
          child: SizedBox(
            height: 32.0,
            child: TextButton(
              onPressed: () {
                Navigator.of(context).pop(); // Close the dialog on cancel button press
              },
              child: const Text('Cancel', style: TextStyle(color: Colors.black87)),
            ),
          ),
        ),
      ],
    );
  }
}
