import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:tc_challenge/widgets/line_chart_widget.dart'; // Importing a custom line chart widget

import '../models/user_account.dart'; // Importing user account model
import '../theme/colors.dart'; // Importing colors from the theme

class LineChart extends StatefulWidget {
  final UserAccount userAccount; // User account data used for the chart

  const LineChart({
    Key? key,
    required this.userAccount,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() => LineChartState();
}

class LineChartState extends State<LineChart> {
  late List<FlSpot> debitSpots1; // List of data points for debits
  late List<FlSpot> creditSpots1; // List of data points for credits

  @override
  void initState() {
    super.initState();
    updateChartData(widget.userAccount); // Initialize chart data when the widget is initialized
  }

  @override
  void didUpdateWidget(LineChart oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (oldWidget.userAccount != widget.userAccount) {
      updateChartData(widget.userAccount); // Update chart data when widget configuration changes
    }
  }

  void updateChartData(UserAccount newUserAccount) {
    // Update the chart data based on the new user account
    setState(() {
      debitSpots1 = newUserAccount.getDebitSpots(); // Retrieve debit data points from user account
      creditSpots1 = newUserAccount.getCreditSpots(); // Retrieve credit data points from user account
    });
  }

  @override
  Widget build(BuildContext context) {
    return AspectRatio(
      aspectRatio: 1.8, // Aspect ratio for the chart container
      child: Stack(
        children: <Widget>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              const SizedBox(
                height: 10,
              ),
              const Text(
                'Account Details',
                style: TextStyle(
                  color: AppColors.pageBackground,
                  fontSize: 18,
                  letterSpacing: 2,
                ),
                textAlign: TextAlign.center,
              ),
              const SizedBox(
                height: 20,
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.only(right: 16, left: 6),
                  child: LineChartWidget( // Custom LineChartWidget used to display the line chart
                    debitSpots: debitSpots1, // Pass debit data points to the custom widget
                    creditSpots: creditSpots1, // Pass credit data points to the custom widget
                  ),
                ),
              ),
              const SizedBox(
                height: 10,
              ),
            ],
          ),
        ],
      ),
    );
  }
}
