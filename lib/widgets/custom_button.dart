import 'package:flutter/material.dart';

import '../theme/colors.dart';

class CustomButton extends StatefulWidget {
  final Function()? onTap; // Callback function triggered when the button is tapped
  final String title; // Text displayed on the button

  const CustomButton({Key? key, required this.onTap, required this.title}) : super(key: key);

  @override
  _CustomButtonState createState() => _CustomButtonState();
}

class _CustomButtonState extends State<CustomButton> {
  bool isPressed = false; // Flag to track whether the button is pressed

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTapDown: (_) {
        setState(() {
          isPressed = true; // Set isPressed to true when the button is pressed
        });
      },
      onTapUp: (_) {
        setState(() {
          isPressed = false; // Set isPressed to false when the button is released
        });
        // Call the provided onTap function when the button is released
        widget.onTap?.call();
      },
      onTapCancel: () {
        setState(() {
          isPressed = false; // Set isPressed to false when the button is canceled
        });
      },
      child: Container(
        padding: const EdgeInsets.all(20),
        margin: const EdgeInsets.symmetric(horizontal: 25),
        decoration: BoxDecoration(
          color: AppColors.contentColorCyanDark.withOpacity(isPressed ? 0.8 : 1.0), // Change opacity based on press state
          borderRadius: BorderRadius.circular(8),
        ),
        child: Center(
          child: Text(
            widget.title, // Display the button text
            style: const TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.bold,
              fontSize: 16,
            ),
          ),
        ),
      ),
    );
  }
}
