import 'package:flutter/material.dart';

class CustomErrorText extends StatelessWidget {
  final String errorText; // The error message to display

  const CustomErrorText({
    Key? key,
    required this.errorText, // Required parameter for error text
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Padding(
        padding: const EdgeInsets.only(top: 5.0, left: 40), // Padding for the error text
        child: Text(
          errorText, // Display the error text
          style: const TextStyle(color: Colors.red, fontSize: 12), // Style for the error text
        ),
      ),
    );
  }
}
