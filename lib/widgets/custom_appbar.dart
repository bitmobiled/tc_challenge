import 'package:flutter/material.dart';

import '../screens/profile_screen.dart';
import 'notification_icon.dart';

class CustomAppBar extends StatelessWidget {
  const CustomAppBar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: Container(
            height: 34,
            padding: const EdgeInsets.symmetric(horizontal: 10),
            margin: const EdgeInsets.only(right: 5),
            decoration: BoxDecoration(
              color: Colors.grey[200], // Background color for search bar
              borderRadius: BorderRadius.circular(8), // Border radius for search bar
            ),
            child: const TextField(
              decoration: InputDecoration(
                hintText: 'Search...', // Placeholder text
                border: InputBorder.none, // Remove input border
                hintStyle: TextStyle(color: Colors.grey), // Hint text color
              ),
            ),
          ),
        ),
        GestureDetector(
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => const ProfileScreen()), // Navigate to ProfileScreen
            );
          },
          child: const Padding(
            padding: EdgeInsets.all(6.0),
            child: CircleAvatar(
              radius: 19, // Radius for the circle avatar
              backgroundImage: AssetImage('lib/assets/images/user_avatar.png'), // Image for the avatar
            ),
          ),
        ),
        const NotificationIcon(), // Custom notification icon widget
      ],
    );
  }
}
