import 'package:flutter/material.dart';

class CustomTextButton extends StatefulWidget {
  final VoidCallback? onTap; // Callback function when the button is tapped
  final TextStyle? textStyle; // Style for the button text
  final String title; // Title/Text displayed on the button

  const CustomTextButton({
    Key? key,
    this.onTap,
    this.textStyle,
    required this.title,
  }) : super(key: key);

  @override
  _CustomTextButtonState createState() => _CustomTextButtonState();
}

class _CustomTextButtonState extends State<CustomTextButton> {
  bool isPressed = false; // Flag to track if the button is pressed

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTapDown: (_) {
        setState(() {
          isPressed = true; // Set the flag to indicate button press
        });
      },
      onTapUp: (_) {
        setState(() {
          isPressed = false; // Reset the flag when the button is released
        });
        // Call the provided onTap function when the button is released
        widget.onTap?.call();
      },
      onTapCancel: () {
        setState(() {
          isPressed = false; // Reset the flag on tap cancellation
        });
      },
      child: Opacity(
        opacity: isPressed ? 0.8 : 1.0, // Adjust opacity when pressed
        child: Text(
          widget.title, // Display the button text
          style: widget.textStyle, // Apply the provided text style
        ),
      ),
    );
  }
}
