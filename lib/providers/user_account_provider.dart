import 'package:flutter/material.dart';
import '../data/user_account_dummy_data.dart';
import '../models/user_account.dart';

class UserAccountProvider with ChangeNotifier {
  List<UserAccount> _userAccounts = [];

  List<UserAccount> get userAccounts =>
      _userAccounts; // Getter for user accounts

  UserAccountProvider() {
    // Use Future.delayed to load dummy data after the widget initialization
    Future.delayed(Duration.zero, () {
      loadDummyData();
    });
  }

  // Method to load dummy data
  void loadDummyData() {
    _userAccounts =
        dummyUserAccountsNew; // Update to use dummyUserAccountsNew for UserAccountNew
    notifyListeners();
  }

  // Method to add a new user account
  void addUserAccount(UserAccount newUserAccount) {
    _userAccounts.insert(0, newUserAccount);
    notifyListeners();
  }

  // Method to remove a user account by index
  void removeUserAccount(int index) {
    _userAccounts.removeAt(index);
    notifyListeners();
  }
}
