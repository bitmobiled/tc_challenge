import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tc_challenge/providers/user_account_provider.dart';
import 'package:tc_challenge/screens/splash_screen.dart';
import 'package:tc_challenge/screens/login_screen.dart';
import 'package:tc_challenge/screens/home_screen.dart';

import 'models/user.dart';

void main() {
  runApp(
    MultiProvider(
      providers: [
        // Provider for managing user data using UserModel
        ChangeNotifierProvider(create: (context) => UserModel()),
        // Provider for managing user account using UserAccountProvider
        ChangeNotifierProvider(create: (context) => UserAccountProvider()),
      ],
      child: const MyApp(),
    ),
  );
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      initialRoute: '/', // Set the initial route

      // Define routes for different screens
      routes: {
        '/': (context) => const SplashScreen(), // Define SplashScreen as initial route
        '/login': (context) => LoginScreen(), // Define LoginPage route
        '/home': (context) => const HomeScreen(), // Define HomePage route
        // Add other routes as needed...
      },
    );
  }
}
