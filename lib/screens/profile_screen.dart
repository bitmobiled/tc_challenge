import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tc_challenge/screens/login_screen.dart';

import '../models/user.dart';
import '../theme/colors.dart';

class ProfileScreen extends StatelessWidget {
  const ProfileScreen({Key? key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Profile'),
        backgroundColor: AppColors.contentColorCyanDark,
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            // Display user avatar
            const CircleAvatar(
              radius: 60,
              backgroundImage: AssetImage('lib/assets/images/user_avatar.png'),
            ),
            const SizedBox(height: 20),

            // Displaying the username from the userModel - login
            Consumer<UserModel>(
              builder: (context, userModel, child) {
                return Text(
                  userModel.username ?? 'Username',
                  style: const TextStyle(
                    fontSize: 24,
                    fontWeight: FontWeight.bold,
                  ),
                );
              },
            ),
            const SizedBox(height: 10),
            const Text(
              'Email: john.doe@example.com',
              style: TextStyle(
                fontSize: 16,
                color: Colors.grey,
              ),
            ),
            const SizedBox(height: 10),
            const Text(
              'First Name: John',
              style: TextStyle(
                fontSize: 16,
                color: Colors.grey,
              ),
            ),
            const SizedBox(height: 10),
            const Text(
              'Last Name: Doe',
              style: TextStyle(
                fontSize: 16,
                color: Colors.grey,
              ),
            ),
            const SizedBox(height: 10),
            // Displaying the phoneNumber from the userModel - login
            Consumer<UserModel>(
              builder: (context, userModel, child) {
                return Text(
                  'Phone: ${userModel.phoneNumber ?? '0000'}',
                  style: const TextStyle(
                    fontSize: 16,
                    color: Colors.blue,
                  ),
                );
              },
            ),
            const SizedBox(height: 20),
            // Logout button
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                padding: const EdgeInsets.symmetric(vertical: 16, horizontal: 30),
                textStyle: const TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                primary: AppColors.contentColorCyanDark,
              ),
              onPressed: () {
                // Logout logic
                Provider.of<UserModel>(context, listen: false).clearUser();

                // Navigate to the login screen
                Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(builder: (context) => LoginScreen()),
                );
              },
              child: const Text('Log out'),
            ),
          ],
        ),
      ),
    );
  }
}
