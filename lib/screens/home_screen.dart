import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tc_challenge/widgets/custom_appbar.dart';
import 'package:tc_challenge/widgets/line_chart.dart';

import '../models/user_account.dart';
import '../providers/user_account_provider.dart';
import '../theme/colors.dart';
import '../widgets/account_card.dart';
import '../widgets/add_account_dialog.dart';
import '../models/user.dart';
import '../utils/greeting.dart';

class CreditDebitRow extends StatelessWidget {
  const CreditDebitRow({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(right: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Row(
            children: [
              const Text(
                'Credit',
                style: TextStyle(fontSize: 12),
              ),
              const SizedBox(width: 8),
              Container(
                width: 10,
                height: 10,
                decoration: const BoxDecoration(
                  color: AppColors.contentColorBlue,
                  shape: BoxShape.circle,
                ),
              ),
            ],
          ),
          const SizedBox(width: 20),
          Row(
            children: [
              const Text(
                'Debit',
                style: TextStyle(fontSize: 12),
              ),
              const SizedBox(width: 8),
              Container(
                width: 10,
                height: 10,
                decoration: const BoxDecoration(
                  color: AppColors.contentColorOrange,
                  shape: BoxShape.circle,
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  List<UserAccount> userAccounts = []; // Empty initially
  UserAccount? selectedAccount;

  @override
  void initState() {
    super.initState();

    // Load data asynchronously when the page is initialized
    fetchData();
  }

  Future<void> fetchData() async {
    final userAccountProviderNew =
        Provider.of<UserAccountProvider>(context, listen: false);

    // Simulate an asynchronous operation
    await Future.delayed(const Duration(milliseconds: 50), () {});

    userAccountProviderNew.loadDummyData(); // Call loadDummyData function to load data

    setState(() {
      userAccounts = userAccountProviderNew.userAccounts;
      if (userAccounts.isNotEmpty) {
        selectedAccount = userAccounts.first;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    final userModel = Provider.of<UserModel>(context);
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(kToolbarHeight + 6),
        child: AppBar(
          backgroundColor: AppColors.contentColorCyanDark,
          elevation: 0,
          automaticallyImplyLeading: false,
          title: const CustomAppBar(),
        ),
      ),
      body: SafeArea(
        child: Column(
          children: [
            const SizedBox(height: 12),
            Padding(
              padding: const EdgeInsets.fromLTRB(20, 0, 20, 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    '${GreetingUtil.getGreeting()}, ${userModel.username}',
                    style: const TextStyle(
                      color: AppColors.pageBackground,
                      fontSize: 18,
                      letterSpacing: 0.2,
                    ),
                  ),
                  FloatingActionButton(
                    onPressed: () {
                      showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return const AddAccountDialog();
                        },
                      );
                    },
                    mini: true,
                    backgroundColor: AppColors.contentColorCyanDark,
                    foregroundColor: Colors.white,
                    elevation: 4.0,
                    splashColor: Colors.grey,
                    child: const Icon(Icons.add),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(8.0, 8, 12, 8),
              child: Column(
                children: [
                  selectedAccount != null
                      ? LineChart(
                          userAccount: selectedAccount!,
                        )
                      : const CircularProgressIndicator(),
                  const CreditDebitRow(),
                ],
              ),
            ),
            const SizedBox(height: 16),
            Expanded(
              child: Consumer<UserAccountProvider>(
                builder: (context, userAccountProviderNew, _) {
                  return userAccounts.isEmpty
                      ? const Center(child: CircularProgressIndicator())
                      : ListView.builder(
                          itemCount: userAccounts.length,
                          itemBuilder: (context, index) {
                            return AccountCard(
                              userAccount: userAccounts[index],
                              onCardPressed: (userAccount) {
                                setState(() {
                                  selectedAccount = userAccount;
                                });
                              },
                            );
                          },
                        );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
