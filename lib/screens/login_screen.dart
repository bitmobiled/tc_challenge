import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../widgets/credentials_error_dialog.dart';
import '../widgets/custom_error_text.dart';
import '../models/user.dart';
import 'home_screen.dart';
import '../widgets/custom_button.dart';
import '../widgets/custom_textfield.dart';
import '../widgets/custom_text_button.dart';
import '../widgets/social_signIn_buttons.dart';
import '../utils/validation_helper.dart';

class LoginScreen extends StatefulWidget {
  LoginScreen({Key? key}) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final usernameController = TextEditingController();
  final passwordController = TextEditingController();

  String usernameError = '';
  String passwordError = '';

  // Function to handle the sign-in process
  void signIn(BuildContext context) async {
    // Validate inputs
    final usernameError = ValidationHelper.validateUsername(usernameController.text);
    final passwordError = ValidationHelper.validatePassword(passwordController.text);

    // Update state with any validation errors
    setState(() {
      this.usernameError = usernameError;
      this.passwordError = passwordError;
    });

    // If usernameError is 'credential_error', set it to an empty string
    if (usernameError == 'credential_error') {
      setState(() {
        this.usernameError = '';
      });
    }

    // If passwordError is 'credential_error', set it to an empty string
    if (passwordError == 'credential_error') {
      setState(() {
        this.passwordError = '';
      });
    }

    // Perform sign-in if no validation errors
    if (usernameError.isEmpty && passwordError.isEmpty) {
      try {
        // Assuming successful sign-in, set the user in UserModel
        final userModel = Provider.of<UserModel>(context, listen: false);
        userModel.setUser(
          username: usernameController.text,
          phoneNumber: '+1234567899',
        );

        // Navigate to the Home page after successful sign-in
        Navigator.pushReplacement(
          context,
          MaterialPageRoute(builder: (context) => const HomeScreen()),
        );
      } catch (error) {
        // Handle sign-in errors
        debugPrint('Sign-in error: $error');
      }
    } else {
      // Show an alert for incorrect username/password combination
      if ((usernameError == 'credential_error' && passwordError == 'credential_error') ||
          (usernameError == 'credential_error' && passwordController.text.isNotEmpty) ||
          (passwordError == 'credential_error' && usernameController.text.isNotEmpty)) {
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return const CredentialsErrorDialog(); // Use the ErrorDialog widget here
          },
        );
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: Center(
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const SizedBox(height: 50),
                Image.asset(
                  'lib/assets/images/topcoder.png',
                  width: 150,
                ),
                const SizedBox(height: 50),
                Text(
                  'Welcome, Let the challenges begin!',
                  style: TextStyle(
                    color: Colors.grey[700],
                    fontSize: 16,
                  ),
                ),
                const SizedBox(height: 20),
                CustomTextField(
                  controller: usernameController,
                  hintText: 'Username',
                  obscureText: false,
                ),
                if (usernameError.isNotEmpty)
                  Row(
                    children: [
                      CustomErrorText(errorText: usernameError),
                    ],
                  ),
                const SizedBox(height: 10),
                CustomTextField(
                  controller: passwordController,
                  hintText: 'Password',
                  obscureText: true,
                ),
                if (passwordError.isNotEmpty)
                  Row(
                    children: [
                      CustomErrorText(errorText: passwordError),
                    ],
                  ),
                const SizedBox(height: 10),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 25.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      CustomTextButton(
                        onTap: () {
                          // Handle the tap event for "Forgot Password"
                          print('Forgot Password tapped!');
                        },
                        textStyle: TextStyle(color: Colors.grey[600]),
                        title: 'Forgot Password?',
                      ),
                    ],
                  ),
                ),
                const SizedBox(height: 25),
                CustomButton(
                  onTap: () {
                    signIn(context);
                  },
                  title: "Sign In",
                ),
                const SizedBox(height: 40),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 25.0),
                  child: Row(
                    children: [
                      Expanded(
                        child: Divider(
                          thickness: 0.5,
                          color: Colors.grey[400],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 10.0),
                        child: Text(
                          'Or continue with',
                          style: TextStyle(color: Colors.grey[700]),
                        ),
                      ),
                      Expanded(
                        child: Divider(
                          thickness: 0.5,
                          color: Colors.grey[400],
                        ),
                      ),
                    ],
                  ),
                ),
                const SizedBox(height: 40),
                const Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SocialSignInButtons(),
                  ],
                ),
                const SizedBox(height: 50),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      'Not a TopCoder member?',
                      style: TextStyle(color: Colors.grey[700]),
                    ),
                    const SizedBox(width: 4),
                    CustomTextButton(
                      onTap: () {
                        // Handle the tap event
                        print('Register now tapped!');
                      },
                      textStyle: const TextStyle(
                        color: Colors.blue,
                        fontWeight: FontWeight.bold,
                      ),
                      title: 'Register now',
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

