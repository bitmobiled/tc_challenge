import 'package:flutter/material.dart';
import 'package:tc_challenge/screens/login_screen.dart';
import 'package:tc_challenge/theme/colors.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key});

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen>
    with SingleTickerProviderStateMixin {
  late AnimationController _animationController;

  @override
  void initState() {
    super.initState();
    _animationController = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 2000),
    );
    _animationController.forward();

    // Navigate to the login page after the animation completes
    _animationController.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        Navigator.of(context).pushReplacement(
          MaterialPageRoute(builder: (_) => LoginScreen()),
        );
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.contentColorCyanDark,
      body: Center(
        child: ScaleTransition(
          scale: _animationController.drive(
            CurveTween(curve: Curves.easeOutBack),
          ),
          child: Image.asset(
            'lib/assets/images/topcoder.png',
            width: 140,
            color: Colors.white,
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }
}



