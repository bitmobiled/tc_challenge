// ValidationHelper class provides static methods to validate username, password, and email fields.
class ValidationHelper {
  static String validateUsername(String username) {
    if (username.isEmpty) {
      return 'Username is required'; // Return an error message for an empty username.
    } else if (username != 'Topcoder') {
      return 'credential_error'; // Return an error message for an invalid username.
    }
    return ''; // Return an empty string if the username is valid.
  }

  static String validatePassword(String password) {
    if (password.isEmpty) {
      return 'Password is required'; // Return an error message for an empty password.
    } else if (password != 'abc123') {
      return 'credential_error'; // Return an error message for an invalid password.
    }
    return ''; // Return an empty string if the password is valid.
  }

  static String validateEmail(String email) {
    if (email.isEmpty) {
      return 'Email is required'; // Return an error message if the email is empty.
    }
    return ''; // Return an empty string if validation passes.
  }
}
