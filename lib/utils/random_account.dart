import 'dart:math';
import '../models/user_account.dart';

// Function to generate a random UserAccount based on the account type
UserAccount generateAccounts(String accountType) {
  // Define minimum and maximum balance values
  const double minBalance = 1000;
  const double maxBalance = 10000;

  // Generate a random available balance between minBalance and maxBalance
  final double randomAvailableBalance =
      minBalance + Random().nextDouble() * (maxBalance - minBalance);

  // Function to generate random transaction amounts between 0 and 4
  double generateRandomAmount() {
    return Random().nextInt(5).toDouble();
  }

  // Create a UserAccount object with random data
  final UserAccount account = UserAccount(
    accountType: accountType,
    availableBalance: randomAvailableBalance,
    // Generate credit and debit transactions for 5 months (from 6 to 10)
    creditTransactions: List.generate(
      5,
          (index) => Transaction(month: index + 6, amount: generateRandomAmount()),
    ),
    debitTransactions: List.generate(
      5,
          (index) => Transaction(month: index + 6, amount: generateRandomAmount()),
    ),
  );

  return account; // Return the generated UserAccount
}
