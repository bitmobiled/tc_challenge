// GreetingUtil class provides a static method to get a greeting message based on the current time.
class GreetingUtil {
  // Method to get a greeting message based on the current time.
  static String getGreeting() {
    final hour = DateTime.now().hour;

    // Check the current hour and return an appropriate greeting message.
    if (hour < 12) {
      return 'Good Morning'; // Return 'Good Morning' before noon.
    } else if (hour < 17) {
      return 'Good Afternoon'; // Return 'Good Afternoon' before evening.
    } else {
      return 'Good Evening'; // Return 'Good Evening' otherwise.
    }
  }
}
