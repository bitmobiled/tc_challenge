import 'package:tc_challenge/utils/random_account.dart';
import '../models/user_account.dart';

// List to store generated dummy user accounts
final List<UserAccount> dummyUserAccountsNew = [
  generateAccounts('Savings'),
  generateAccounts('Current'),
  generateAccounts('Fixed Deposit'),
  generateAccounts('Nominated'),
  generateAccounts('Personal'),
];


