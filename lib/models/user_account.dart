import 'package:fl_chart/fl_chart.dart';

// Transaction class representing a transaction with a month and amount
class Transaction {
  final int month; // Month of the transaction
  final double amount; // Amount of the transaction

  Transaction({required this.month, required this.amount}); // Constructor for Transaction
}

// UserAccount class representing a user account with account details and transactions
class UserAccount {
  final String accountType; // Type of the account
  final double availableBalance; // Available balance in the account
  final List<Transaction> creditTransactions; // List of credit transactions
  final List<Transaction> debitTransactions; // List of debit transactions

  // Constructor for UserAccount
  UserAccount({
    required this.accountType,
    required this.availableBalance,
    required this.creditTransactions,
    required this.debitTransactions,
  });

  // Method to get FlSpot data for credit transactions to be used in LineChart
  List<FlSpot> getCreditSpots() {
    return creditTransactions
        .map((transaction) => FlSpot(transaction.month.toDouble(), transaction.amount))
        .toList();
  }

  // Method to get FlSpot data for debit transactions to be used in LineChart
  List<FlSpot> getDebitSpots() {
    return debitTransactions
        .map((transaction) => FlSpot(transaction.month.toDouble(), transaction.amount))
        .toList();
  }
}
