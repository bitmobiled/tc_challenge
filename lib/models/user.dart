import 'package:flutter/material.dart';

/// UserModel class represents user data and account information.
/// It extends ChangeNotifier to notify listeners about changes in user data.
class UserModel extends ChangeNotifier {
  String? username;
  String? phoneNumber;

  /// Method to set user information.
  /// Updates the [username] and [phoneNumber] properties with provided data.
  /// Notifies listeners about the changes in user data.
  void setUser({required String username, required String phoneNumber}) {
    this.username = username;
    this.phoneNumber = phoneNumber;
    notifyListeners(); // Notify listeners of the change
  }

  /// Method to clear user information (logout).
  /// Sets both [username] and [phoneNumber] to null.
  /// Notifies listeners about this change.
  void clearUser() {
    username = null;
    phoneNumber = null;
    notifyListeners(); // Notify listeners of the change
  }
}
