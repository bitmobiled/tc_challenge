import 'package:flutter/material.dart';

import '../screens/home_screen.dart';
import '../screens/login_screen.dart';

// Class for managing app routes
class AppRoutes {
  // Method to generate routes based on route settings
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
    // Route for the initial screen, showing the LoginScreen
      case '/':
        return MaterialPageRoute(builder: (_) => LoginScreen());
    // Route for the home screen
      case '/home':
        return MaterialPageRoute(builder: (_) => const HomeScreen());
    // Default route in case no matching route is found
      default:
        return MaterialPageRoute(
          builder: (_) => Scaffold(
            body: Center(
              child: Text('No route defined for ${settings.name}'),
            ),
          ),
        );
    }
  }
}
