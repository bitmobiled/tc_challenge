# tc_challenge - Topcoder - Flutter Dart Fun Challenge

## Valid Login Credentials
username - Topcoder
password - abc123

phone no: +1234567899

### Validation Video Link
Link: https://drive.google.com/file/d/1JOKZPLeETfSnDILgu-IEjhU9AhpNMs8W/view?usp=drive_link

## Accounts
create dummy data for user account

## Project Structure
lib
├── screens
│   ├── home_screen.dart
│   ├── profile_screen.dart
│   ├── login_screen.dart
│   ├── splash_screen.dart
│   └── ...
├── widgets
│   ├── account_card.dart
│   ├── add_account_dialog.dart
│   ├── credentials_error_dialog.dart
│   ├── custom_appbar.dart
│   ├── custom_button.dart
│   ├── custom_error_text.dart
│   ├── custom_text_button.dart
│   ├── custom_text_field.dart
│   ├── line_chart.dart
│   ├── line_chart_widget.dart
│   ├── notification_icon.dart
│   ├── social_signin_buttons.dart
│   ├── square_tile.dart
│   └── ...
├── models
│   ├── user_account.dart
│   ├── user.dart
│   └── ...
├── providers
│   ├── user_account_provider.dart
│   └── ...
├── utils
│   ├── greeting.dart
│   ├── random_account.dart
│   ├── validation_helper.dart
│   └── ...
├── theme
│   ├── style.dart
│   ├── colors.dart
│   └── ...
├── routes
│   ├──app_routes.dart
│   └── ...
├── data
│   ├──user_account_dummy_data.dart
│   └── ...
├── assets
│   ├── images
│   ├── fonts
│   └── ...
├── main.dart
└── ...



